﻿using System;

namespace UcPag
{
    public class Juro
    {
        private double valorInicial;
        private int tempo;
        private double final;

        public Juro() { }
        public Juro(double ini, int tp)
        {
            valorInicial = ini;
            tempo = tp;
        }
        public double Result
        {
            get { return final; }
        }
        public double GerarTaxaJuro()
        {
            return 0.01;
        }
        public void CalculaJuros(double ini, int tp)
        {
            if (ini > 0 && tp > 0 && tp < 13)
            {
                  final = Math.Round(ini * Math.Pow((1 + GerarTaxaJuro()), tp),2);
                
            }
            else
            {
                throw new ArgumentOutOfRangeException("ZERO ai");

            }
        }

        public static void Main()
        {
            Juro juro = new Juro();
            juro.CalculaJuros(100, 5);
            Console.WriteLine(juro.final);
        }
    }
}
