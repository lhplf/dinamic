<?php
class Administrador{
    public $id;
    public $nome;
    public $email;
    public $login;
    public $senha;

    // public function getId(){
    //     return $this->id;
    // }

    // public function setId($value){
    //     $this->$id=$value;
    // }

    // public function getNome(){
    //     return $this->nome;
    // }

    // public function setNome($value){
    //     $this->nome=$value;
    // }

    // public function getEmail(){
    //     return $this->id;
    // }

    // public function setEmail($value){
    //     $this->email=$value;
    // }

    // public function getSenha(){
    //     return $this->senha;
    // }

    // public function setSenha($value){
    //     $this->senha=$value;
    // }

    public static function loadById($id_adm){
        $sql = new SQL();
        $resultado = $sql->select("select * from administrador where id = :id", array(":id"=>$id_adm));
        if (count($resultado)>0) {
            return $resultado[0];
        }
    }

    public static function getList(){
        $sql = new SQL();
        return $sql->select("select * from administrador order by nome");
    }

    public static function search($adm){
        $sql = new SQL();
        return $sql->select("select * from administrador where nome like :nome", array(":nome"=>"%".$adm."%"));
    }

    public function login($_login,$_senha){
        $senhacriptografada = md5($_senha);
        $sql = new SQL();
        $resultado = $sql->select("select * from administrador where login = :login and senha = :senha", array(":login"=>$_login,":senha"=>$senhacriptografada));
        if(count($resultado)>0){
            $this->setData($resultado[0]);
        }

    }

    public static function setData($data){
        $id = ($data['id']);
        $nome = ($data['nome']);
        $email = ($data['email']);
        $GLOBALS = ($data['login']);
        $senha = ($data['senha']);
    }

    public function insert(){
        $sql = new SQL();
        $resultado = $sql->select("call sp_adm_insert(:nome,:email,:login,:senha)", 
        array(
            ":nome"=>$this->nome,
            ":email"=>$this->email,
            ":login"=>$this->login,
            ":senha"=>$this->senha
        ));
        if (count($resultado) > 0) {
            //$this->setData($resultado[0]);
            return $resultado[0];
        }
    }

    public function update($_id,$_nome,$_email,
    $_senha){
        // $this->nome = $_nome;
        // $this->senha = $_senha;
        $sql = new SQL();
        $sql->query("UPDATE administrador SET nome = :nome, email = :email, senha = :senha WHERE id = :id", 
        array(
            ":id"=>$_id,
            ":nome"=>$_nome,
            "email"=>$_email,
            ":senha"=>md5($_senha)
        ));
    }

    public function delete(){
        $sql = new SQL();
        $sql->query("DELETE FROM administrador WHERE id = :id",
        array(":id"=>$this->id));
    }
    //criando métodos construtores no PHP

    public function __construct($nome="",$email ="",$login="",$senha =""){
        $this->nome=$nome;
        $this->email=$email;
        $this->login=$login;
        $this->senha=$senha;
    }

    public function __toString(){

        return json_encode(array(
            ":id"=>$this->id,
            ":nome"=>$this->nome,
            ":email"=>$this->email,
            ":login"=>$this->login,
            ":senha"=>$this->senha
        ));
    }
}
?>