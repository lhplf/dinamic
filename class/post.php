<?php
class Banner{
    public $id_post;
    public $id_categoria;
    public $titulo;
    public $descricao;
    public $imagem;
    public $visitas;
    public $data;
    public $ativo;    

    public function __construct($_id="",$_idcategoria="",$_titulo_post="",$_descricao="",$_imagem="",$_visitas="",$_data="",$_ativo=""){
        $this->id_categoria=$_id;
        $this->link=$_idcategoria;
        $this->titulo=$_titulo_post;
        $this->descricao=$_descricao;
        $this->imagem=$_imagem;
        $this->visitas=$_visitas;
        $this->data=$_data;
        $this->ativo=$_ativo;
    }

    public static function loadById($id_post){
        $sql = new SQL();
        $resultado = $sql->select("select * from post where id = :id", array(":id"=>$id_post));
        if (count($resultado)>0) {
            return $resultado[0];
        }
    }

    public static function getList(){
        $sql = new SQL();
        return $sql->select("select * from post order by titulo_post");
    }

    public static function search($post){
        $sql = new SQL();
        return $sql->select("select * from post where nome like :titulo_post", array(":titulo_banner"=>"%".$post."%"));
    }

    public static function setData($data){
        $id_post = ($data['id_post']);
        $id_categoria = ($data['id_categoria']);
        $titulo = ($data['titulo_post']);
        $descricao = ($data['descricao_post']);
        $imagem = ($data['img_post']);
        $visitas = ($data['visitas']);
        $data = ($data['data_post']);
        $ativo = ($data['post_ativo']);
    }

    public function insert(){
        $sql = new SQL();
        $resultado = $sql->select("call sp_post_insert(:id_categoria,:titulo_post,:descricao_post,:img_post,:visitas,:data_post,:post_ativo)", 
        array(
            ":id_categoria"=>$this->id_categoria,
            ":titulo_post"=>$this->titulo,
            ":descricao_post"=>$this->descricao,
            ":img_post"=>$this->imagem,
            ":visitas"=>$this->visitas,
            ":data_post"=>$this->data,
            ":post_ativo"=>$this->ativo
        ));
        if (count($resultado) > 0) {
            return $resultado[0];
        }
    }

    public function update($_id,$_idcategoria,$_titulo_post,$_descricao,$_imagem,$_visitas,$_data,$_ativo){
        $sql = new SQL();
        $sql->query("UPDATE post SET id_categoria = :id_categoria, titulo_post = :titulo_post, descricao_post = :descricao_post, img_post = :img_post, visitas = :visitas,data_post = :data_post,post_ativo = :post_ativo WHERE id = :id", 
        array(
            ":id"=>$_id,
            ":titulo_post"=>$_titulo_post,
            ":descricao_post"=>$_descricao,
            ":img_post"=>$_imagem,
            ":visitas"=>$_visitas,
            ":data_post"=>$_data,
            ":post_ativo"=>$_ativo
        ));
    }

    public function delete(){
        $sql = new SQL();
        $sql->query("DELETE FROM post WHERE id = :id",array(
            ":id"=>$this->id_post
        ));
    }


    // public function __toString(){

    //     return json_encode(array(
    //         ":id_post"=>$this->$_id,
    //         ":id_categoria"=>$this->$_id,
    //         ":titulo_post"=>$this->$_titulo_post,
    //         ":descricao_post"=>$this->$_descricao,
    //         ":img_post"=>$this->$_imagem,
    //         ":visitas"=>$this->$_visitas,
    //         ":data_post"=>$this->$_data,
    //         ":post_ativo"=>$this->$_ativo
    //     ));
    // }
}
?>