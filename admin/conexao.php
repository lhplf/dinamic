<?php
    require_once("../conf.php");

    // Conexão banco de dados MySql (Simples)

    // $host = "localhost";
    // $usuario = "root";
    // $senha = "usbw";
    // $banco = "dinamicodb";
    // $db = mysqli_connect($host,$usuario,$senha,$banco);
    // PDO - Orientação a Objetos(PHP DATA OBJECT)

    $sql = "mysql:host=".IP_SERVER_DB."; dbname=".NOME_BANCO.";";
    $dns_opt = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

    try { 
        $conn = new PDO($sql, USER_DB, PASS_DB, $dns_opt);
        //echo "Sucesso na Conexão!";
    } catch (PDOException $error) {
        echo 'Erro de conexão: ' . $error->getMessage();
    }
?>