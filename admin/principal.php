<?php
    session_start();
    $nome_adm = isset($_SESSION['nome_adm'])?$_SESSION['nome_adm']:'bla';
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Área Administrativa | <?php echo $nome_adm;?> </title>
    <link rel="stylesheet" href="../css/style_admin.css">
</head>
<body>
    <div id="principal">
        <div id="cabecalho">
            <div id="titulo_top">
                <img src="img/adm.png" alt="">
            </div>
        </div>
    </div>
    <div id="corpo">
        <div id="esquerdo">
            <div id="sessao">Categoria
                <ul>
                    <li><a href="principal.php?link=2">Cadastrar</a></li>
                    <li><a href="principal.php?link=3">Listar/Editar</a></li>
                </ul>
            </div>
            <div id="sessao">Noticias
                <ul>
                    <li><a href="principal.php?link=4">Cadastrar</a></li>
                    <li><a href="principal.php?link=5">Listar/Editar</a></li>
                </ul>
            </div>
            <div id="sessao">Post
                <ul>
                    <li><a href="principal.php?link=6">Cadastrar</a></li>
                    <li><a href="principal.php?link=7">Listar/Editar</a></li>
                </ul>
            </div>
            <div id="sessao">Banner
                <ul>
                    <li><a href="principal.php?link=8">Cadastrar</a></li>
                    <li><a href="principal.php?link=9">Listar/Editar</a></li>
                </ul>
            </div>
            <div id="sessao">Administrador
                <ul>
                    <li><a href="principal.php?link=10">Cadastrar</a></li>
                    <li><a href="principal.php?link=11">Listar/Editar</a></li>
                </ul>
            </div>
        </div>
        <div id="direito">
            <?php
                    $link = $_GET['link'];

                    $pag[1] = "home.php";
                    $pag[2] = "frm_categoria.php";
                    $pag[3] = "lista_categoria.php";
                    $pag[4] = "frm_noticia.php";
                    $pag[5] = "lista_noticia.php";
                    $pag[6] = "frm_post.php";
                    $pag[7] = "lista_post.php";
                    $pag[8] = "frm_banner.php";
                    $pag[9] = "lista_banner.php";
                    $pag[10] = "frm_administrador.php";
                    $pag[11] = "lista_administrador.php";
                    if(!empty($link)){
                        if(file_exists($pag[$link])){
                            include $pag[$link];
                        }
                        else {
                        include "home.php"; 
                        }
                    }
                    else {
                        include "home.php"; 
                    }
                ?>
        </div>
    </div>
</body>
</html>