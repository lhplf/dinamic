<?php
    //Inicializar sessão do usuário
    //session_start();

    //definindo padrão de zona GMT(Hora do mundo) -3,00: hr de brasilia
    date_default_timezone_set('America/Sao_Paulo');

    //inicia as classes
    spl_autoload_register(function($nome_classe){
        $nome_arquivo = "../class".DIRECTORY_SEPARATOR.$nome_classe.".php";
        if (file_exists($nome_arquivo)) {
            require_once($nome_arquivo);
        }
    });

    //inicializar variáveis de banco
    define('IP_SERVER_DB', '127.0.0.1');
    define('HOSTNAME','ITQ0626037W10-1');
    define('NOME_BANCO', 'dinamicodb');
    define('USER_DB', 'root');
    define('PASS_DB','usbw');

?>