<?php
    require_once('../admin/conexao.php');

    if (isset($_POST['cadastro'])) {
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $foto = $_FILES['foto'];

        if(!empty($foto['name'])){
            // largura máxima em pixels
            $largura = 640;
            //altura máxima em pixels
            $altura = 425;
            //tamanho máximo em bytes
            $tamanho = 300000;

            $error = array();
            // verifica se é uma imagem
            if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
                $error[1] = "Isso não é uma imagem!";
            }
            //recuperando as dimensões da imagem
            $dimensoes = getimagesize($foto['tmp_name']);
            //var_dump($dimensoes);

            //verifica se a largura da imagem é maior que a permitida 
            if ($dimensoes[0]>$largura) {
                $error[2] = "A largura da imagem é maior que a permitida! Não deve ultrapassar " .$largura. " pixels.";
            } 

            //verifica se a altura da imagem é maior que a permitida 
            if ($dimensoes[1]>$altura) {
                $error[3] = "A altura da imagem é maior que a permitida! Não deve ultrapassar " .$altura. " pixels.";
            } 

            //verifica se o tamanho da imagem é maior que a permitida 
            if($foto['size']>$tamanho){
                $error[4] = "O tamanho da imagem é maior que o permitido! Não deve ultrapassar " .$tamanho. " bytes.";
            }

            //se não houver erros
            if(count($error)==0){
                //recupera a extensão do arquivo de imagem
                // /^image\/(pjpeg|jpeg|png|gif|bmp)$/
                preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'],$ext);
                // gera um nome de arquivo unico para a imagem
                $nome_imagem =md5(uniqid(time())). ".".$ext[1];
                // caminho para armazenar as imagens 
                $caminho_imagem = "foto/".$nome_imagem;
                //upload da imagem a partir do espaço temporário  
                move_uploaded_file($foto['tmp_name'],$caminho_imagem);
                //insere os dados do usuário no banco
                $cmd = $conn->prepare("insert into usuario (nome,email,foto) values(:nome,:email,:foto)");
                $resultado = $cmd->execute(array(
                    ":nome" =>  $nome,
                    ":email" =>  $email,
                    ":foto" =>  $nome_imagem
                ));
                var_dump($resultado);

                echo "<br /> Usuário inserido com sucesso.";
            }
        }
        if (count($error)!= 0) {
            foreach ($error as $erro) {
                echo $erro . "<br />";
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro de Usuário</title>
</head>
<body>
    <h1>Novo Usuário</h1>
    <form action="<?php echo $_SERVER['PHP_SELF']?>" method ="post" enctype="multipart/form-data" name="cadastro_form">
        Nome: <br>
        <input type="text" name="nome" required><br><br>
        Email: <br>
        <input type="email" name="email" required><br><br>
        Foto de exibição: <br>
        <input type="file" name="foto"><br><br>
        <input type="submit" name="cadastro" value="Cadastrar">
    </form>
    <br>
    <br>
    <hr>
    <h2>Usuários Cadastrados</h2>
    <?php
        $cmd = $conn->prepare("select * from usuario");
        $cmd->execute();
        $resultado = $cmd->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($resultado);
        foreach($resultado as $usuario){
            echo "<img src = 'foto/".$usuario['foto']."' width='56px' height ='48px'>";
            echo "<br />";
            echo "Nome: ".$usuario['nome'];
            echo "<br />";
            echo "Email : ".$usuario['email'];
            echo "<br />";
        }
    ?>
</body>
</html>